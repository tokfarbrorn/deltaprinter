
#include "steppers.h"

Target *currentTarget;

inline static void loadNextTarget()
{
  if(targetRingBufferEnd != targetRingBufferStart)
  {
    currentTarget = &targetRingBuffer[targetRingBufferStart];
  }
  else
  {
     currentTarget = NULL; 
  }
}

ISR(TIMER3_OVF_vect)          // interrupt service routine that wraps a user defined function supplied by attachInterrupt
{
  
  if(currentTarget == NULL)
    return;
    
  if(currentT1Pos < desiredT1Pos)
  {
    digitalWrite(Z_STEP_PIN, HIGH);
    currentT1Pos++;
    digitalWrite(Z_STEP_PIN, LOW);
  }
  else if(currentT1Pos > desiredT1Pos)
  {
    digitalWrite(Z_STEP_PIN, HIGH);
    currentT1Pos--;
    digitalWrite(Z_STEP_PIN, LOW);
  }
  
  if(currentT2Pos < desiredT2Pos)
  {
    digitalWrite(Y_STEP_PIN, HIGH);
    currentT2Pos++;
    digitalWrite(Y_STEP_PIN, LOW);
  }
  else if(currentT2Pos > desiredT2Pos)
  {
    digitalWrite(Y_STEP_PIN, HIGH);
    currentT2Pos--;
    digitalWrite(Y_STEP_PIN, LOW);
  }
  
  if(currentT3Pos < desiredT3Pos)
  {
    digitalWrite(X_STEP_PIN, HIGH);
    currentT3Pos++;
    digitalWrite(X_STEP_PIN, LOW);
  }
  else if(currentT3Pos > desiredT3Pos)
  {
    digitalWrite(X_STEP_PIN, HIGH);
    currentT3Pos--;
    digitalWrite(X_STEP_PIN, LOW);
  }

}

ISR(TIMER1_OVF_vect)          // interrupt service routine that wraps a user defined function supplied by attachInterrupt
{
  
  if(currentTarget == NULL)
    loadNextTarget();
  
  if(currentTarget == NULL)
    return;

  if(currentTarget->fraction >= 1)
  {
    targetRingBufferStart = (targetRingBufferStart + 1) % targetRingBufferSize;
    loadNextTarget();
  }
  
  if(currentTarget == NULL)
    return;

  currentTarget->fraction += currentTarget->fractionPerInterval;
  
  double nextX = currentTarget->startX + (currentTarget->deltaX * currentTarget->fraction);
  double nextY = currentTarget->startY + (currentTarget->deltaY * currentTarget->fraction);
  double nextZ = currentTarget->startZ + (currentTarget->deltaZ * currentTarget->fraction);
  
  double t1 = sqrt(DELTA_DIAGONAL_ROD_2 - sq(DELTA_TOWER1_X - nextX) - sq(DELTA_TOWER1_Y - nextY)) + nextZ;
  double t2 = sqrt(DELTA_DIAGONAL_ROD_2 - sq(DELTA_TOWER2_X - nextX) - sq(DELTA_TOWER2_Y - nextY)) + nextZ;
  double t3 = sqrt(DELTA_DIAGONAL_ROD_2 - sq(DELTA_TOWER3_X - nextX) - sq(DELTA_TOWER3_Y - nextY)) + nextZ;

  desiredT1Pos = TO_STEPS(t1);
  desiredT2Pos = TO_STEPS(t2);
  desiredT3Pos = TO_STEPS(t3);

  if(desiredT1Pos < currentT1Pos)
    digitalWrite(Z_DIR_PIN, LOW);
  else
    digitalWrite(Z_DIR_PIN, HIGH);

  if(desiredT2Pos < currentT2Pos)
    digitalWrite(Y_DIR_PIN, LOW);
  else
    digitalWrite(Y_DIR_PIN, HIGH);

  if(desiredT3Pos < currentT3Pos)
    digitalWrite(X_DIR_PIN, LOW);
  else
    digitalWrite(X_DIR_PIN, HIGH);

}
  
