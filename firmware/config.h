#define SIN_60 0.8660254037844386
#define COS_60 0.5

// Center-to-center distance of the holes in the diagonal push rods.
#define DELTA_DIAGONAL_ROD 283.0 // mm

// Horizontal offset from middle of printer to smooth rod center.
#define DELTA_SMOOTH_ROD_OFFSET 198.0 // mm

// Horizontal offset of the universal joints on the end effector.
#define DELTA_EFFECTOR_OFFSET 50.0 // mm

// Horizontal offset of the universal joints on the carriages.
#define DELTA_CARRIAGE_OFFSET 32.0 // mm

#define DELTA_RADIUS (DELTA_SMOOTH_ROD_OFFSET-DELTA_EFFECTOR_OFFSET-DELTA_CARRIAGE_OFFSET)
#define DELTA_DIAGONAL_ROD_2 sq(DELTA_DIAGONAL_ROD)

#define DELTA_TOWER1_X -SIN_60 * DELTA_RADIUS // front left tower
#define DELTA_TOWER1_Y -COS_60 * DELTA_RADIUS
#define DELTA_TOWER2_X SIN_60 * DELTA_RADIUS // front right tower
#define DELTA_TOWER2_Y -COS_60 * DELTA_RADIUS
#define DELTA_TOWER3_X 0.0 // back middle tower
#define DELTA_TOWER3_Y DELTA_RADIUS

#define STEPS_PER_MM 80


#define TO_STEPS(x)  x * STEPS_PER_MM

#define HOME_Z_POS 500
