
#include "cb.h"
#include "config.h"
#include "pins.h"
#include "steppers.h"

double lastX = 0;
double lastY = 0;
double lastZ = 0;

void setup()
{
  
  pinMode(X_ENABLE_PIN, OUTPUT);
  pinMode(Y_ENABLE_PIN, OUTPUT);
  pinMode(Y_ENABLE_PIN, OUTPUT);

  pinMode(X_STEP_PIN, OUTPUT);
  pinMode(Y_STEP_PIN, OUTPUT);
  pinMode(Z_STEP_PIN, OUTPUT);

  pinMode(X_DIR_PIN, OUTPUT);
  pinMode(Y_DIR_PIN, OUTPUT);
  pinMode(Z_DIR_PIN, OUTPUT);
  
  pinMode(X_MAX_PIN, INPUT);
  pinMode(Y_MAX_PIN, INPUT);
  pinMode(Z_MAX_PIN, INPUT);
  
  digitalWrite(X_ENABLE_PIN, HIGH);
  digitalWrite(Y_ENABLE_PIN, HIGH);
  digitalWrite(Z_ENABLE_PIN, HIGH);
  
  Serial.begin(115200);
  Serial.println("Pommbot v1");
  
  TCCR1A = 0;                 // clear control register A 
  TCCR1B = _BV(WGM13);        // set mode 8: phase and frequency correct pwm, stop the timer

  TCCR3A = 0;                 // clear control register A 
  TCCR3B = _BV(WGM13);        // set mode 8: phase and frequency correct pwm, stop the timer

  long cyclesMain = (F_CPU / 2000000) * 1000;
  long cyclesStep = (F_CPU / 2000000) * 62;
  
  int oldSREG = SREG;				
  cli();							// Disable interrupts for 16 bit register access
  ICR1 = cyclesMain;                                          // ICR1 is TOP in p & f correct pwm mode
  ICR3 = cyclesStep;                                          // ICR1 is TOP in p & f correct pwm mode
  SREG = oldSREG;
  
  TCCR1B &= ~(_BV(CS10) | _BV(CS11) | _BV(CS12));
  TCCR1B |= _BV(CS10); 
  
  TCCR3B &= ~(_BV(CS10) | _BV(CS11) | _BV(CS12));
  TCCR3B |= _BV(CS10); 

  TIMSK1 = _BV(TOIE1);
  TIMSK3 = _BV(TOIE1);
  
  
}

void
powerOnSteppers()
{
  digitalWrite(X_ENABLE_PIN, LOW);
  digitalWrite(Y_ENABLE_PIN, LOW);
  digitalWrite(Z_ENABLE_PIN, LOW);
}

void
targetLinear(char *args, double *values, int argc)
{

  while((targetRingBufferEnd + 1) % targetRingBufferSize == targetRingBufferStart);
  
  powerOnSteppers();
  
  Target *t = &targetRingBuffer[targetRingBufferEnd];
  
  double endX = lastX;
  double endY = lastY;
  double endZ = lastZ;
  
  for(int i = 0; i < argc; i++)
  {
    if(args[i] == 'X')
      endX = values[i];   
    if(args[i] == 'Y')
      endY = values[i];   
    if(args[i] == 'Z')
      endZ = values[i];   
  }
    
  t->startX = lastX;
  t->startY = lastY;
  t->startZ = lastZ;
   
  t->targetX = lastX = endX;
  t->targetY = lastY = endY;
  t->targetZ = lastZ = endZ;

  t->deltaX = t->targetX - t->startX;
  t->deltaY = t->targetY - t->startY;
  t->deltaZ = t->targetZ - t->startZ;

  double distance = sqrt(sq(t->deltaX) + sq(t->deltaY) + sq(t->deltaZ));
  // constant speed of 50 mm / sec
  
  t->duration = distance / 50;

  t->fractionPerInterval = 1.0 / t->duration / 1000.0;

  /*
  Serial.print("Adding move to x=");
  Serial.print(t->targetX);
  Serial.print(", y=");
  Serial.print(t->targetY);  
  Serial.print(", z=");
  Serial.print(t->targetZ);
  Serial.print(" from x=");
  Serial.print(t->startX);
  Serial.print(", y=");
  Serial.print(t->startY);  
  Serial.print(", z=");
  Serial.println(t->startZ);
  Serial.print("Distance ");
  Serial.println(distance);
  Serial.print("Duration ");
  Serial.println(t->duration);
  */
  
  targetRingBufferEnd = (targetRingBufferEnd + 1) % targetRingBufferSize;

}

void
halt()
{
  digitalWrite(X_ENABLE_PIN, HIGH);
  digitalWrite(Y_ENABLE_PIN, HIGH);
  digitalWrite(Z_ENABLE_PIN, HIGH);
}

void
home()
{
  while(digitalRead(X_MAX_PIN) == 1 || digitalRead(Y_MAX_PIN) == 1 || digitalRead(Z_MAX_PIN) == 1)
  {
    digitalWrite(X_ENABLE_PIN, LOW);
    digitalWrite(Y_ENABLE_PIN, LOW);
    digitalWrite(Z_ENABLE_PIN, LOW);
    digitalWrite(Z_DIR_PIN, HIGH);
    digitalWrite(Y_DIR_PIN, HIGH);
    digitalWrite(X_DIR_PIN, HIGH);
    if(digitalRead(X_MAX_PIN) == 1)
    {
      digitalWrite(X_STEP_PIN, HIGH);
      digitalWrite(X_STEP_PIN, LOW);
    }
    if(digitalRead(Y_MAX_PIN) == 1)
    {
      digitalWrite(Y_STEP_PIN, HIGH);
      digitalWrite(Y_STEP_PIN, LOW);
    }
    if(digitalRead(Z_MAX_PIN) == 1)
    {
      digitalWrite(Z_STEP_PIN, HIGH);
      digitalWrite(Z_STEP_PIN, LOW);
    }
    delay(1);
  }
  currentT1Pos = TO_STEPS(HOME_Z_POS);
  currentT2Pos = TO_STEPS(HOME_Z_POS);
  currentT3Pos = TO_STEPS(HOME_Z_POS);
}

char serialBuffer[255];
unsigned int serialBufferPosition = 0;

void loop()
{
  // put your main code here, to run repeatedly:
  
  while(Serial.available())
  {
    serialBuffer[serialBufferPosition] = Serial.read();
    if(serialBuffer[serialBufferPosition] == '\n' || serialBuffer[serialBufferPosition] == '\r')
    {
      if(serialBufferPosition > 1)
      {
        parseCommand();
      }
      serialBufferPosition = 0;
    }
    else
    {      
      serialBufferPosition++;
    }
  }

}

int
read_int(char *str, unsigned int *value)
{
  int i = 0;
  int mantissa = 0;
  char *s = str;
  while(s[0] >= '0' && s[0] <= '9')
  {
    mantissa = (mantissa << 3) + (mantissa << 1) + (s[0] - '0');
    *value = mantissa;
    s++; i++;
  }
  return(i);
}

int
read_digit(char *str, double *floatValue)
{
  int i = 0;
  int mantissa = 0;
  char *s = str;
  char _str[20];
  while((s[0] >= '0' && s[0] <= '9') || s[0] == '.' || s[0] == '-')
  {
    _str[i++] = s[0];
    s++;
  }
  _str[i] = '\0';
  *floatValue = strtod(_str, (char **) NULL);
  return(i);
}

void
parseCommand()
{

  char cmd = serialBuffer[0];
  unsigned int num = 0;
  char args[5] = { '\0', '\0', '\0', '\0', '\0' };
  double argValues[5] = { 0, 0, 0, 0, 0 };
  unsigned int numValues = 0;
  
  int i = 1;
  i += read_int(serialBuffer + i , &num);
  
  for(; i < serialBufferPosition; i++)
  {
    switch(serialBuffer[i])
    {
       case ';':
        i = serialBufferPosition; // skip to end
        break;
       case ' ':
         continue;
       case 'X':
       case 'Y':
       case 'Z':
         args[numValues] = serialBuffer[i];
         i += read_digit(serialBuffer + i + 1, &argValues[numValues]);
         numValues++; 
         break;
       default:
         Serial.println("parse error");
         break;
    }   
  }
  
  if(cmd == 'G')
  {
    if(num == 1)
    {
      targetLinear(args, argValues, numValues);
      return;
    }
    Serial.println("invalid command"); 
  }
  
  //digitalWrite(X_ENABLE_PIN, LOW);
  //digitalWrite(Y_ENABLE_PIN, LOW);
  //digitalWrite(Z_ENABLE_PIN, LOW);

  
}

