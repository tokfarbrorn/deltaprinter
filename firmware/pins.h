
    #define X_STEP_PIN         54
    #define X_DIR_PIN          55
    #define X_ENABLE_PIN       38
    #define X_MIN_PIN           3
    #define X_MAX_PIN           2

    #define Y_STEP_PIN         60
    #define Y_DIR_PIN          61
    #define Y_ENABLE_PIN       56
    #define Y_MIN_PIN          14
    #define Y_MAX_PIN          15

    #define Z_STEP_PIN         46
    #define Z_DIR_PIN          48
    #define Z_ENABLE_PIN       62
    #define Z_MIN_PIN          18
    #define Z_MAX_PIN          19

    #define Y2_STEP_PIN        36
    #define Y2_DIR_PIN         34
    #define Y2_ENABLE_PIN      30

    #define Z2_STEP_PIN        36
    #define Z2_DIR_PIN         34
    #define Z2_ENABLE_PIN      30

    #define E0_STEP_PIN        26
    #define E0_DIR_PIN         28
    #define E0_ENABLE_PIN      24

    #define E1_STEP_PIN        36
    #define E1_DIR_PIN         34
    #define E1_ENABLE_PIN      30

    #define SDPOWER            -1
    #define SDSS               53
    #define LED_PIN            13
    
    #define FAN_PIN            9 // (Sprinter config)
    #define KILL_PIN           41

    #define HEATER_1_PIN       -1
    #define HEATER_2_PIN       -1 

    #define TEMP_0_PIN         13   // ANALOG NUMBERING
    #define TEMP_1_PIN         15   // ANALOG NUMBERING
    #define TEMP_2_PIN         -1   // ANALOG NUMBERING

    #define HEATER_BED_PIN     8    // BED
 
    #define TEMP_BED_PIN       14   // ANALOG NUMBERING

    #define BEEPER 37

    #define BTN_EN1 31
    #define BTN_EN2 33
    #define BTN_ENC 35

    #define SDCARDDETECT 49

