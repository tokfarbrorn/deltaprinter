
#include "config.h"

#ifndef _STEPPERS
#define _STEPPERS

typedef struct _target
{
  
  double targetX;
  double targetY;
  double targetZ;
  
  double startX;
  double startY;
  double startZ;
    
  double deltaX;
  double deltaY;
  double deltaZ;

  double fraction;
  double fractionPerInterval;
  
  double duration;
    
} Target;

volatile unsigned int currentT1Pos = TO_STEPS(258);
volatile unsigned int currentT2Pos = TO_STEPS(258);
volatile unsigned int currentT3Pos = TO_STEPS(258);

volatile unsigned int desiredT1Pos = currentT1Pos;
volatile unsigned int desiredT2Pos = currentT2Pos;
volatile unsigned int desiredT3Pos = currentT3Pos;

#endif
